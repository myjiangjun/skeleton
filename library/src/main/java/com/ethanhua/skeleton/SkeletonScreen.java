package com.ethanhua.skeleton;

/**
 * Created by ethanhua on 2017/7/29.
 *
 * @since 2021-04-16
 */
public interface SkeletonScreen {
    /**
     * show
     */
    void show();

    /**
     * hide
     */
    void hide();
}
