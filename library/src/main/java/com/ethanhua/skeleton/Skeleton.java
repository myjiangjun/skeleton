package com.ethanhua.skeleton;

import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;

/**
 * Created by ethanhua on 2017/7/29.
 *
 * @since 2021-04-16
 */
public class Skeleton {
    /**
     * RecyclerViewSkeletonScreen.Builder
     *
     * @param recyclerView
     * @return RecyclerViewSkeletonScreen.Builder
     */
    public static RecyclerViewSkeletonScreen.Builder bind(ListContainer recyclerView) {
        return new RecyclerViewSkeletonScreen.Builder(recyclerView);
    }

    /**
     * ViewSkeletonScreen.Builder
     *
     * @param view
     * @return ViewSkeletonScreen.Builder
     */
    public static ViewSkeletonScreen.Builder bind(ComponentContainer view) {
        return new ViewSkeletonScreen.Builder(view);
    }
}
