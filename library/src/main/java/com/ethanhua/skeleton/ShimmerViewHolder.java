package com.ethanhua.skeleton;

import io.supercharge.shimmerlayout.ShimmerLayout;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * Created by ethanhua on 2017/7/29.
 *
 * @since 2021-04-16
 */
public class ShimmerViewHolder {
    static ShimmerLayout getComponent(LayoutScatter layoutScatter, ComponentContainer parent, int innerViewResId) {
        ShimmerLayout cptShimmer = (ShimmerLayout) layoutScatter.parse(
                ResourceTable.Layout_layout_shimmer, parent, false);
        ComponentContainer cptInnerView = (ComponentContainer)layoutScatter.parse(
                innerViewResId, parent, false);
        ComponentContainer.LayoutConfig lp = cptInnerView.getLayoutConfig();
        if (lp != null) {
            cptShimmer.setLayoutConfig(lp);
        }
        cptShimmer.setComponentContainer(cptInnerView);
        return cptShimmer;
    }
}
