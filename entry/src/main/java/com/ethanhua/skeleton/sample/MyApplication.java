package com.ethanhua.skeleton.sample;

import ohos.aafwk.ability.AbilityPackage;

/**
 * MyApplication
 *
 * @since 2021-04-16
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
