package com.ethanhua.skeleton.sample.slice;

import com.ethanhua.skeleton.sample.ResourceTable;
import com.ethanhua.skeleton.sample.page.StatusPage;
import com.ethanhua.skeleton.sample.view.MaterialRippleLayout;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

/**
 * StatusViewSlice
 *
 * @since 2021-04-16
 */
public class StatusViewSlice extends AbilitySlice {
    private static final int TITLE_COLOR = -13615201;
    private static final float RIPPLE_ALPHA = 0.2f;
    private static final int RIPPLE_DURATION = 2000;
    private static final int RIPPLE_DIAMETERDP = 20;

    private Button content;
    private Button empty;
    private Button error;
    private Button loading;
    private PageSlider ps;
    private DirectionalLayout skl;
    private MaterialRippleLayout materialRippleLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_ability_status_view);
        initId();

        StatusPage statusPage = new StatusPage(getContext(), "");
        ps.setProvider(statusPage);

        startAnimator(loading);
        materialRippleLayout.setOnClick(new MaterialRippleLayout.OnClickButton() {
            @Override
            public void onClick() {
                StatusPage statusPage = new StatusPage(getContext(), "loading");
                ps.setProvider(statusPage);
            }
        });

        startAnimator(error);
        materialRippleLayout.setOnClick(new MaterialRippleLayout.OnClickButton() {
            @Override
            public void onClick() {
                StatusPage statusPage = new StatusPage(getContext(), "error");
                ps.setProvider(statusPage);
            }
        });

        startAnimator(empty);
        materialRippleLayout.setOnClick(new MaterialRippleLayout.OnClickButton() {
            @Override
            public void onClick() {
                StatusPage statusPage = new StatusPage(getContext(), "empty");
                ps.setProvider(statusPage);
            }
        });

        startAnimator(content);
        materialRippleLayout.setOnClick(new MaterialRippleLayout.OnClickButton() {
            @Override
            public void onClick() {
                StatusPage statusPage = new StatusPage(getContext(), "content");
                ps.setProvider(statusPage);
            }
        });
    }

    private void initId() {
        ps = (PageSlider) findComponentById(ResourceTable.Id_ps);
        loading = (Button) findComponentById(ResourceTable.Id_bt_loading);
        error = (Button) findComponentById(ResourceTable.Id_btn_error);
        empty = (Button) findComponentById(ResourceTable.Id_btn_empty);
        content = (Button) findComponentById(ResourceTable.Id_btn_content);
        skl = (DirectionalLayout) findComponentById(ResourceTable.Id_skl);
    }

    private void startAnimator(Button bt) {
        if (bt != null) {
            materialRippleLayout = new MaterialRippleLayout(this).on(bt)
                    .rippleColor(Color.getIntColor("#86666666"))
                    .rippleAlpha(RIPPLE_ALPHA)
                    .rippleDuration(RIPPLE_DURATION)
                    .rippleBackground(Color.getIntColor("#D1D1D1"))
                    .rippleOverlay(true)
                    .rippleDiameterDp(RIPPLE_DIAMETERDP)
                    .create();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
