package com.ethanhua.skeleton.sample;

import com.ethanhua.skeleton.sample.slice.ListContainerViewSlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * ListContainerView
 *
 * @since 2021-04-16
 */
public class ListContainerView extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ListContainerViewSlice.class.getName());
    }
}
