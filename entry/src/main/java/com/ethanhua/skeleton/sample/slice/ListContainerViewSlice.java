package com.ethanhua.skeleton.sample.slice;

import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;
import com.ethanhua.skeleton.sample.ResourceTable;
import com.ethanhua.skeleton.utils.DensityUtils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * ListContainerViewSlice
 *
 * @since 2021-04-16
 */
public class ListContainerViewSlice extends AbilitySlice {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ListContainerViewSlice");
    private static final String TYPE_GRID = "type_grid";
    private static final String TYPE_LINEAR = "type_linear";
    private static final String PARAMS_TYPE = "params_type";
    private static final int TITLE_COLOR = -13615201;
    private static final int ANGLE = 20;
    private static final int DURATION = 1200;
    private static final int COUNT = 10;
    private static final int TIME = 3000;
    private static final int COLUMNCOUNT = 2;
    private String mType;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_ability_recyclerview);

        mType = intent.getStringParam(PARAMS_TYPE);
        initView();
    }

    private void initView() {
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_lc_view);
        if (TYPE_LINEAR.equals(mType)) {
            NewsProvider adapter = new NewsProvider(this);
            final SkeletonScreen skeletonScreen = Skeleton.bind(listContainer)
                    .adapter(adapter)
                    .shimmer(true)
                    .angle(ANGLE)
                    .frozen(false)
                    .duration(DURATION)
                    .count(COUNT)
                    .color(Color.getIntColor("#EAEAEA"))
                    .load(ResourceTable.Layout_item_skeleton_news)
                    .show();

            HiLog.info(LABEL, "getMainTaskDispatcher() start ");
            getMainTaskDispatcher().delayDispatch(new Runnable() {
                @Override
                public void run() {
                    skeletonScreen.hide();
                    HiLog.info(LABEL, "getMainTaskDispatcher()");
                }
            }, TIME);
            HiLog.info(LABEL, "getMainTaskDispatcher() end");
        } else if (TYPE_GRID.equals(mType)) {
            TableLayoutManager layoutManager = new TableLayoutManager();
            layoutManager.setColumnCount(COLUMNCOUNT);
            layoutManager.setOrientation(Component.HORIZONTAL);
            listContainer.setLayoutManager(layoutManager);

            int width = DensityUtils.getDisplayWidth(getContext());
            listContainer.setWidth(width);
            PersonProvider personProvider = new PersonProvider(this);
            final SkeletonScreen skeletonScreen = Skeleton.bind(listContainer)
                    .adapter(personProvider)
                    .color(Color.getIntColor("#EAEAEA"))
                    .load(ResourceTable.Layout_item_skeleton_person)
                    .shimmer(true)
                    .show();

            HiLog.info(LABEL, "getMainTaskDispatcher() start ");
            ListContainerViewSlice.this.getMainTaskDispatcher().delayDispatch(new Runnable() {
                @Override
                public void run() {
                    skeletonScreen.hide();
                    HiLog.info(LABEL, "getMainTaskDispatcher()");
                }
            }, TIME);
            HiLog.info(LABEL, "getMainTaskDispatcher() end");
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
