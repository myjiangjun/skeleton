package com.ethanhua.skeleton.sample;

import com.ethanhua.skeleton.sample.slice.ViewAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * ViewAbility
 *
 * @since 2021-04-16
 */
public class ViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ViewAbilitySlice.class.getName());
    }
}
