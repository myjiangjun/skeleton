package com.ethanhua.skeleton.sample.slice;

import com.ethanhua.skeleton.sample.ResourceTable;
import com.ethanhua.skeleton.sample.view.MaterialRippleLayout;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * MainAbilitySlice
 *
 * @since 2021-04-16
 */
public class MainAbilitySlice extends AbilitySlice {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MainAbilitySlice");

    private static final String PARAMS_TYPE = "params_type";
    private static final int TITLE_COLOR = -13615201;
    private static final String BUNDLENAME = "com.ethanhua.skeleton.sample";
    private static final String GOOLE = "goole";
    private static final float RIPPLE_ALPHA = 0.2f;
    private static final int RIPPLE_DURATION = 2000;
    private static final int RIPPLE_DIAMETERDP = 35;
    private Button btnList;
    private Button btnGrid;
    private Button btnView;
    private Button btnImgloading;
    private Button btnStatus;
    private MaterialRippleLayout materialRippleLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initId();
        startAnimator(btnList);
        materialRippleLayout.setOnClick(() -> {
            Intent intentList = new Intent();
            startIntent(intentList, PARAMS_TYPE, "type_linear", "com.ethanhua.skeleton.sample.ListContainerView");
        });

        startAnimator(btnGrid);
        materialRippleLayout.setOnClick(() -> {
            Intent intentGrid = new Intent();
            startIntent(intentGrid, PARAMS_TYPE, "type_grid", "com.ethanhua.skeleton.sample.ListContainerView");
        });

        startAnimator(btnView);
        materialRippleLayout.setOnClick(() -> {
            Intent mIntent = new Intent();
            mIntent.setParam(PARAMS_TYPE, "type_view");
            startIntent(mIntent, PARAMS_TYPE, "type_view", "com.ethanhua.skeleton.sample.ViewAbility");
        });

        startAnimator(btnImgloading);
        materialRippleLayout.setOnClick(new MaterialRippleLayout.OnClickButton() {
            @Override
            public void onClick() {
                Intent mIntent = new Intent();
                mIntent.setParam(PARAMS_TYPE, "type_img");
                startIntent(mIntent, PARAMS_TYPE, "type_img", "com.ethanhua.skeleton.sample.ViewAbility");
            }
        });

        startAnimator(btnStatus);
        materialRippleLayout.setOnClick(new MaterialRippleLayout.OnClickButton() {
            @Override
            public void onClick() {
                Intent mIntent = new Intent();
                mIntent.setParam(GOOLE, GOOLE);
                startIntent(mIntent, PARAMS_TYPE, GOOLE, "com.ethanhua.skeleton.sample.StatusViewAbility");
            }
        });
    }

    private void initId() {
        btnList = (Button) findComponentById(ResourceTable.Id_btn_list);
        btnGrid = (Button) findComponentById(ResourceTable.Id_btn_grid);
        btnView = (Button) findComponentById(ResourceTable.Id_btn_view);
        btnImgloading = (Button) findComponentById(ResourceTable.Id_btn_Imgloading);
        btnStatus = (Button) findComponentById(ResourceTable.Id_btn_status);
    }

    /**
     * 启动动画
     *
     * @param bt
     */
    private void startAnimator(Button bt) {
        if (bt != null) {
            materialRippleLayout = new MaterialRippleLayout(this).on(bt)
                    .rippleColor(Color.getIntColor("#86666666"))
                    .rippleAlpha(RIPPLE_ALPHA)
                    .rippleDuration(RIPPLE_DURATION)
                    .rippleBackground(Color.getIntColor("#D1D1D1"))
                    .rippleOverlay(true)
                    .rippleDiameterDp(RIPPLE_DIAMETERDP)
                    .create();
        }
    }

    private void startIntent(Intent intent, String key, String value, String abilityName) {
        intent.setParam(key, value);
        Operation mOperation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(BUNDLENAME)
                .withAbilityName(abilityName)
                .build();
        intent.setOperation(mOperation);
        startAbility(intent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
