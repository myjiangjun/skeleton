package com.ethanhua.skeleton.sample.page;

import com.ethanhua.skeleton.sample.ResourceTable;
import com.ethanhua.skeleton.sample.view.ProgressWheel;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * StatusPage
 *
 * @since 2021-04-16
 */
public class StatusPage extends PageSliderProvider {
    private static final int BARLENGTH = 260;
    private static final int BARWIDTH = 10;
    private static final int RIMWIDTH = 10;
    private static final int SPINSPEED = 50;
    private Context context;
    private String string;
    private ProgressWheel pwOne;

    /**
     * StatusPage
     *
     * @param context
     * @param string
     */
    public StatusPage(Context context, String string) {
        this.context = context;
        this.string = string;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        ComponentContainer parse = null;
        if ("loading".equals(string)) {
            parse = (ComponentContainer) LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_layout_progress, null, false);
            pwOne = (ProgressWheel) parse.findComponentById(ResourceTable.Id_pw);
            pwOne.setBarColor(Color.getIntColor("#FF3E96"));
            pwOne.setRimColor(Color.getIntColor("#ffffff"));
            pwOne.setBarLength(BARLENGTH);
            pwOne.setBarWidth(BARWIDTH);
            pwOne.setRimWidth(RIMWIDTH);
            pwOne.setSpinSpeed(SPINSPEED);

            // 启动动画旋转
            pwOne.startSpinning();
        } else if ("error".equals(string)) {
            parse = (ComponentContainer) LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_layout_error, null, false);
        } else if ("empty".equals(string)) {
            parse = (ComponentContainer) LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_layout_empty_view, null, false);
        } else if ("content".equals(string) || string.equals("")) {
            parse = (ComponentContainer) LayoutScatter.getInstance(context)
                    .parse(ResourceTable.Layout_item_content, null, false);
        }

        componentContainer.addComponent(parse);
        return parse;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return false;
    }
}
