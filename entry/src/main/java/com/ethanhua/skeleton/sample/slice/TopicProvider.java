package com.ethanhua.skeleton.sample.slice;

import com.ethanhua.skeleton.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

import java.util.Collections;

/**
 * TopicProvider
 *
 * @since 2021-04-16
 */
public class TopicProvider extends BaseItemProvider {
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_CONTENT = 2;
    private static final int COUNT = 7;

    private AbilitySlice slice;

    /**
     * TopicProvider
     *
     * @param slice
     */
    public TopicProvider(AbilitySlice slice) {
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Override
    public Object getItem(int i) {
        return Collections.emptyList();
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public int getItemComponentType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }
        return TYPE_CONTENT;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (getItemComponentType(i) == TYPE_HEADER) {
            return LayoutScatter.getInstance(slice)
                    .parse(ResourceTable.Layout_item_title_more, componentContainer, false);
        }
        return LayoutScatter.getInstance(slice)
                .parse(ResourceTable.Layout_item_topic, componentContainer, false);
    }
}
