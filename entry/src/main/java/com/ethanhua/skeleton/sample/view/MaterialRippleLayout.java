/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ethanhua.skeleton.sample.view;

import com.ethanhua.skeleton.sample.TypedAttrUtils;

import ohos.accessibility.ability.GesturePathDefine;
import ohos.accessibility.ability.GesturePathPositionDefine;
import ohos.accessibility.ability.GestureResultListener;
import ohos.accessibility.ability.GestureResultListenerInfo;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentParent;
import ohos.agp.components.ComponentState;
import ohos.agp.components.ListContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * MaterialRippleLayout
 *
 * @since 2021-04-16
 */
public class MaterialRippleLayout extends StackLayout
        implements Component.TouchEventListener,Component.DrawTask,Component.ClickedListener {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MaterialRippleLayout");
    private static final int DEFAULT_LONG_PRESS_TIMEOUT = 500;
    private static final boolean IS_DEFAULT_DELAY_CLICK = true;
    private static final float DEFAULT_ALPHA = 0.2f;
    private static final int DEFAULT_DURATION = 350;
    private static final float DEFAULT_DIAMETER_DP = 80;
    private static final int HOVER_DURATION = 1500;
    private static final int ADD_RADIUS_LENGTH = 20;
    private int rippleDuration = DEFAULT_DURATION;
    private static final boolean IS_DEFAULT_RIPPLE_OVERLAY = false;
    private static final Color DEFAULT_BACKGROUND = Color.TRANSPARENT;
    private static final int DEFAULT_RIPPLE_COLOR = 0x80000000;
    private static final int INTAGER2 = 2;
    private static final int INTAGER_SIX = 6;
    private static final int INTAGER3 = 3;
    private static final int TIME = 200;

    MmiPoint pointerPosition;
    DelayRunAnimator delayRunAnimator = new DelayRunAnimator();

    private final AnimatorValue.ValueUpdateListener mAnimatorUpdateListener = new AnimatorValue.ValueUpdateListener() {
        @Override
        public void onUpdate(AnimatorValue animatorV, float value) {
            if (value <= 0) {
                return;
            }
            double currentRadius = rippleDiameter + maxRadius * value;
            if (currentRadius < rippleDiameter) {
                setRadius(rippleDiameter);
            } else {
                setRadius(currentRadius);
            }
            invalidate();
        }
    };

    private Component childView;
    private Color rippleColor = Color.BLACK;
    private float rippleAlpha = DEFAULT_ALPHA;
    private float rippleDiameter = DEFAULT_DIAMETER_DP;
    private boolean isRippleOverlay;
    private boolean isRippleInAdapter;
    private ShapeElement mBackgroundUnable;
    private ShapeElement mBackgroundPressed;

    private final Paint mPaint = new Paint();

    private EventHandler handler;

    private AnimatorValue animatorValue;

    private ListContainer parentAdapter;

    private final Rect bounds = new Rect();
    private boolean isLongClick = false;
    private float maxRadius;
    private double mRadiusRipple;
    private boolean isFingerUp = true;
    private float positionX;
    private double positionY;
    private OnClickButton mclickButton;

    /**
     * MaterialRippleLayout
     *
     * @param context
     */
    public MaterialRippleLayout(Context context) {
        super(context);
        init(context, null, "");
    }

    /**
     * MaterialRippleLayout
     *
     * @param context
     * @param attrSet
     */
    public MaterialRippleLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet, "");
    }

    /**
     * MaterialRippleLayout
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public MaterialRippleLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet, styleName);
    }

    public float getX() {
        return positionX;
    }

    public void setX(float ppx) {
        this.positionX = ppx;
    }

    public double getY() {
        return positionY;
    }

    public void setY(double ppy) {
        this.positionY = ppy;
    }

    public double getRadius() {
        return mRadiusRipple;
    }

    public void setRadius(double radius) {
        this.mRadiusRipple = radius;
    }

    /**
     * RippleBuilder on(Button view)
     *
     * @param view
     * @return RippleBuilder
     */
    public RippleBuilder on(Button view) {
        return new RippleBuilder(view);
    }

    private void init(Context context, AttrSet attrs, String defStyleAttr) {
        handler = new EventHandler(EventRunner.getMainEventRunner());

        initAttrs(context, attrs, defStyleAttr);
        setupPaint();

        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
    }

    private void initAttrs(Context context, AttrSet attrSet, String defStyleAttr) {
        setStatePress();
        if (attrSet == null) {
            return;
        }

        rippleColor = TypedAttrUtils.getColor(attrSet, "mrl_rippleColor", new Color(DEFAULT_RIPPLE_COLOR));
        rippleAlpha = TypedAttrUtils.getFloat(attrSet, "mrl_rippleAlpha", DEFAULT_ALPHA);
        rippleDuration = TypedAttrUtils.getInteger(attrSet, "mrl_rippleDuration", DEFAULT_DURATION);
        rippleDiameter = TypedAttrUtils.getFloat(attrSet, "mrl_rippleDiameterDp", DEFAULT_DIAMETER_DP);

        isRippleOverlay = TypedAttrUtils.getBoolean(attrSet, "mrl_rippleOverlay", IS_DEFAULT_RIPPLE_OVERLAY);

        HiLog.info(LABEL, "initAttrs rippleDiameter = " + rippleDiameter);
    }

    /**
     * 初始化画笔
     */
    private void setupPaint() {
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(rippleColor);
        mPaint.setAlpha(rippleAlpha);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        HiLog.info(LABEL, "initAttrs onDraw getWidth = " + getWidth() + "   getRadius() : " + getRadius());
        canvas.drawCircle(getX(), (float) getY(), (float) getRadius(), mPaint);
    }

    private void setStatePress() {
        mBackgroundUnable = new ShapeElement();
        mBackgroundUnable.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
        mBackgroundPressed = new ShapeElement();
        mBackgroundPressed.setRgbColor(RgbColor.fromArgbInt(Color.BLUE.getValue()));
        int[][] states = new int[INTAGER_SIX][];
        states[0] = new int[]{ComponentState.COMPONENT_STATE_DISABLED};
        states[1] = new int[]{ComponentState.COMPONENT_STATE_PRESSED};
        states[INTAGER2] = new int[]{ComponentState.COMPONENT_STATE_FOCUSED};
        states[INTAGER3] = new int[]{ComponentState.COMPONENT_STATE_EMPTY};
        StateElement stateBackground = new StateElement();
        stateBackground.addState(states[0], mBackgroundUnable);
        stateBackground.addState(states[1], mBackgroundPressed);
        stateBackground.addState(states[INTAGER2], mBackgroundPressed);
        stateBackground.addState(states[INTAGER3], mBackgroundUnable);
    }

    /**
     * 点击事件的动画
     */
    private void initAnimator() {
        animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animatorValue.setDuration(rippleDuration);
        animatorValue.setValueUpdateListener(mAnimatorUpdateListener);
        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                setRadius(0);
                fingerUp();

                if (mclickButton != null) {
                    mclickButton.onClick();
                }
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        animatorValue.start();
    }

    public void setOnClick(OnClickButton clickButton) {
        mclickButton = clickButton;
    }

    /**
     * RippleBuilder on(Button view)
     *
     * @since 2021-04-16
     */
    public interface OnClickButton {
        /**
         * onClick
         */
        void onClick();
    }

    /**
     * 长按事件动画
     */
    private void initLongClickAnimator() {
        animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animatorValue.setDuration(rippleDuration);
        animatorValue.setValueUpdateListener(mAnimatorUpdateListener);
        animatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                if (isRippleOverlay && isFingerUp) {
                    setRadius(0);
                    invalidate();
                }
                if (!isRippleOverlay) {
                    setRadius(0);
                    invalidate();
                }
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        animatorValue.start();
    }

    /**
     * RippleBuilder on(Button view)
     *
     * @param <T>
     * @return Component
     */
    @SuppressWarnings("unchecked")
    public <T extends Component> T getChildView() {
        return (T) childView;
    }

    @Override
    public void addComponent(Component childComponent) {
        if (getChildCount() > 0) {
            throw new IllegalStateException("");
        }
        childView = childComponent;
        super.addComponent(childComponent);
    }

    @Override
    public void setClickedListener(ClickedListener listener) {
        if (childView == null) {
            throw new IllegalStateException("");
        }
        childView.setClickedListener(listener);
    }

    @Override
    public void setLongClickedListener(LongClickedListener listener) {
        if (childView == null) {
            throw new IllegalStateException("");
        }
        childView.setLongClickedListener(listener);
    }

    @Override
    public void onClick(Component component) {
        isLongClick = false;
        rippleDuration = DEFAULT_DURATION;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        touchEvent.getRadius(0);
        pointerPosition = touchEvent.getPointerPosition(0);
        float ppx = pointerPosition.getX();
        int[] parentLocationOnScreen = getLocationOnScreen();

        setX(ppx - parentLocationOnScreen[0]);
        setY((double) ((float)component.getHeight() / (float) INTAGER2));
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                isFingerUp = false;
                rippleDuration = DEFAULT_DURATION;
                handler.postTask(delayRunAnimator, TIME);
                setRippleDuration(rippleDuration);

                if (Math.abs(ppx - parentLocationOnScreen[0])
                        > Math.abs(ppx - (parentLocationOnScreen[0] + getWidth()))) {
                    maxRadius = Math.abs(ppx - parentLocationOnScreen[0]) + ADD_RADIUS_LENGTH;
                } else {
                    maxRadius = Math.abs(ppx - (parentLocationOnScreen[0] + getWidth())) + ADD_RADIUS_LENGTH;
                }
                mRadiusRipple = maxRadius;
                fingerPress();
                resetConfig();
                return true;
            case TouchEvent.POINT_MOVE:
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                isFingerUp = true;
                if (!isLongClick) {
                    handler.removeTask(delayRunAnimator);
                    if (rippleDuration <= 0) {
                        rippleDuration = DEFAULT_DURATION;
                    }
                    setRippleDuration(rippleDuration);
                    initAnimator();
                }
                if (isLongClick) {
                    fingerUp();
                }
                break;
            default:
                break;
        }
        return false;
    }

    /**
     * DelayRunAnimator
     *
     * @since 2021-04-16
     */
    private class DelayRunAnimator implements Runnable {
        @Override
        public void run() {
            isLongClick = true;
            rippleDuration = HOVER_DURATION;
            initLongClickAnimator();
        }
    }

    /**
     * 点击操作
     */
    private void performalViewClick() {
        if (getComponentParent() instanceof ListContainer) {
            if (!childView.callOnClick()) {
                clickAdapterView((ListContainer) getComponentParent());
            }
        } else if (isRippleInAdapter) {
            clickAdapterView(findParentAdapterView());
        } else {
            if (childView != null) {
                childView.callOnClick();
            }
        }
    }

    private void clickAdapterView(ListContainer parent) {
        final int position = parent.getIndexForComponent(MaterialRippleLayout.this);
        final long itemId = parent.getItemProvider() != null
                ? parent.getItemProvider().getItemId(position)
                : 0;
        if (position != ListContainer.INVALID_INDEX) {
            parent.executeItemClick(MaterialRippleLayout.this, position, itemId);
        }
    }

    /**
     * PerformClickEvent
     *
     * @since 2021-04-16
     */
    private class PerformClickEvent implements Runnable {
        @Override
        public void run() {
            if (getComponentParent() instanceof ListContainer) {
                if (!childView.callOnClick()) {
                    clickAdapterView((ListContainer) getComponentParent());
                }
            } else if (isRippleInAdapter) {
                clickAdapterView(findParentAdapterView());
            } else {
                if (childView != null) {
                    childView.callOnClick();
                }
                callOnClick();
            }
        }

        private void clickAdapterView(ListContainer parent) {
            final int position = parent.getIndexForComponent(MaterialRippleLayout.this);
            final long itemId = parent.getItemProvider() != null
                    ? parent.getItemProvider().getItemId(position)
                    : 0;
            if (position != ListContainer.INVALID_INDEX) {
                parent.executeItemClick(MaterialRippleLayout.this, position, itemId);
            }
        }
    }

    private ListContainer findParentAdapterView() {
        if (parentAdapter != null) {
            return parentAdapter;
        }
        ComponentParent current = getComponentParent();
        while (true) {
            if (current instanceof ListContainer) {
                parentAdapter = (ListContainer) current;
                return parentAdapter;
            } else {
                if (current.getComponentParent() != null) {
                    current = current.getComponentParent();
                }
            }
        }
    }

    private void resetConfig() {
        setRadius(getRadius());
    }

    /**
     * 按下
     */
    private void fingerPress() {
    }

    /**
     * 抬起
     */
    private void fingerUp() {
        if (isRippleOverlay && isFingerUp) {
            setRadius(0);
            invalidate();
        }
        isLongClick = false;
    }

    /**
     * setRippleColor
     *
     * @param rippleColor
     */
    public void setRippleColor(int rippleColor) {
        this.rippleColor = new Color(rippleColor);
        mPaint.setColor(new Color(rippleColor));
        mPaint.setAlpha(rippleAlpha);
        invalidate();
    }

    /**
     * setDefaultRippleAlpha
     *
     * @param alpha
     */
    public void setDefaultRippleAlpha(float alpha) {
        this.rippleAlpha = alpha;
        mPaint.setAlpha(rippleAlpha);
        invalidate();
    }

    /**
     * isRippleDelayClick
     *
     * @param isClick
     */
    public void isRippleDelayClick(boolean isClick) {
    }

    public void setRippleDiameter(int rippleDiameter) {
        this.rippleDiameter = rippleDiameter;
    }

    public void setRippleDuration(int rippleDuration) {
        this.rippleDuration = rippleDuration;
    }

    /**
     * isRippleOverlay
     *
     * @param isOverlay
     */
    public void isRippleOverlay(boolean isOverlay) {
        this.isRippleOverlay = isOverlay;
    }

    /**
     * setRippleBackground
     *
     * @param color
     */
    public void setRippleBackground(RgbColor color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(color);
        setBackground(shapeElement);
    }

    /**
     * RippleBuilder
     *
     * @since 2021-04-16
     */
    public static class RippleBuilder {
        private static final int RED = 255;
        private static final int GREEN = 255;
        private static final int BLUE = 255;
        private final Context context;
        private final Component child;

        private int rippleColor = Color.rgb(0, 0, 0);
        private boolean isRippleOverlay = IS_DEFAULT_RIPPLE_OVERLAY;
        private float rippleDiameter = DEFAULT_DIAMETER_DP;
        private int rippleDuration = DEFAULT_DURATION;
        private float rippleAlpha = DEFAULT_ALPHA;
        private boolean isRippleDelayClick = IS_DEFAULT_DELAY_CLICK;
        private int rippleBackground = Color.rgb(RED, GREEN, BLUE);

        /**
         * RippleBuilder
         *
         * @param child
         */
        public RippleBuilder(Button child) {
            this.child = child;
            this.context = child.getContext();
        }

        /**
         * rippleColor
         *
         * @param color
         * @return RippleBuilder
         */
        public RippleBuilder rippleColor(int color) {
            this.rippleColor = color;
            return this;
        }

        /**
         * rippleOverlay
         *
         * @param isOverlay
         * @return RippleBuilder
         */
        public RippleBuilder rippleOverlay(boolean isOverlay) {
            this.isRippleOverlay = isOverlay;
            return this;
        }

        /**
         * rippleDiameterDp
         *
         * @param diameterDp
         * @return RippleBuilder
         */
        public RippleBuilder rippleDiameterDp(int diameterDp) {
            this.rippleDiameter = diameterDp;
            return this;
        }

        /**
         * rippleDuration
         *
         * @param duration
         * @return RippleBuilder
         */
        public RippleBuilder rippleDuration(int duration) {
            this.rippleDuration = duration;
            return this;
        }

        /**
         * rippleAlpha
         *
         * @param alpha
         * @return RippleBuilder
         */
        public RippleBuilder rippleAlpha(float alpha) {
            this.rippleAlpha = alpha;
            return this;
        }

        /**
         * rippleDelayClick
         *
         * @param isDelayClick
         * @return RippleBuilder
         */
        public RippleBuilder rippleDelayClick(boolean isDelayClick) {
            this.isRippleDelayClick = isDelayClick;
            return this;
        }

        /**
         * rippleBackground
         *
         * @param color
         * @return RippleBuilder
         */
        public RippleBuilder rippleBackground(int color) {
            this.rippleBackground = color;
            return this;
        }

        /**
         * create
         *
         * @return MaterialRippleLayout
         */
        public MaterialRippleLayout create() {
            MaterialRippleLayout layout = new MaterialRippleLayout(context);
            layout.setRippleColor(rippleColor);
            layout.setDefaultRippleAlpha(rippleAlpha);
            layout.isRippleDelayClick(isRippleDelayClick);
            layout.setRippleDiameter(AttrHelper.fp2px(rippleDiameter, context));
            layout.setRippleDuration(rippleDuration);
            layout.isRippleOverlay(isRippleOverlay);
            layout.setRippleBackground(new RgbColor(rippleBackground));

            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(rippleBackground));
            child.setBackground(shapeElement);

            ComponentContainer.LayoutConfig params = child.getLayoutConfig();
            ComponentContainer parent = (ComponentContainer) child.getComponentParent();
            int index = 0;

            if (parent instanceof MaterialRippleLayout) {
                throw new IllegalStateException("");
            }

            if (parent != null) {
                index = parent.getChildIndex(child);
                parent.removeComponent(child);
            }

            layout.addComponent(child,
                    new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                            ComponentContainer.LayoutConfig.MATCH_PARENT));

            if (parent != null) {
                parent.addComponent(layout, index, params);
            }
            return layout;
        }
    }
}
